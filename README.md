PAM-notify-telegram
===================

#### Pluggable Authentication pam-exec telegram login/ssh/sudo notification script

![alt text](pam-notify-screenshot.png "Title")

* expects `/etc/telegram-notify.pam.conf` and `telegram-notify` ( http://www.bernaerts-nicolas.fr/linux/75-debian/351-debian-send-telegram-notification )

## **Installation:**

* **clone** this repo `mkdir /etc/custom/ ; git clone https://gitlab.com/the-foundation/pam-notify-telegram.git /etc/custom/pam-notify-telegram`

* **install** `telegram-notify` and `curl`

  **OR** use the one from this repository
  (`ln -s /etc/custom/pam-notify-telegram/telegram-notify /usr/local/bin/telegram-notify`)
  
* **create** `/etc/telegram-notify.pam.conf`

  ```
  # -------------------------------#
  #   /etc/telegram-notify.conf    #
  # -------------------------------#

  [general]
  api-key=112233:MicCheckYøYøImG0nnaBurnTh1sT0kenThough
  user-id=

  [network]
  socks-proxy=

  ```
* **talk** to your bot by sending a message beginning with slash e.g. `/sayHello`

 ( or add it to multi user conversation and then talk)

* **extract** the chatID by using `setup-bot.sh`

 **or** manually (`curl  -d "offset=0" https://api.telegram.org/bot${api_key}/getUpdates` )

* if you have      pam.d/common-session e.g. UBUNTU  : **add** the following line to **BOTH FILES** `/etc/pam.d/common-session`  `/etc/pam.d/sudo`
* if youre missing pam.d/common-session e.g. UBUNTU  : **add** the following line to **BOTH FILES** `/etc/pam.d/sshd`  `/etc/pam.d/sudo`

 `session    optional     pam_exec.so /etc/custom/pam-notify-telegram/pam-notify-telegram`

* non-debian or secured environments may need to add `ALL ALL=(root) NOPASSWD: /bin/bash /etc/custom/pam-notify-telegram/pam-notify-telegram.sh` to `etc/sudoders`

* user `git` and `su` commands are filtered unless you touch `/etc/pam-notify-su`
* you may disable notifications for users by putting one name per line in `/etc/ssh-no-login-notify`


## Debugging
* modify the pam config lines like this:

 `session    optional     pam_exec.so debug log=/tmp/pam-notify.log /etc/custom/pam-notify-telegram/pam-notify-telegram`

 and run `tail -f /tmp/pam-notify.log `


 ---
## USING webhoks2telegram 
### ATTENTION: the webhook2telegram  was renamed( to telepush ) and refactored, the PAM-script is tested with all versions
### but relies on the fact that the newest version "telepush" returns an error when using the "old" api/messages endpoint
### docker image tags: v1 n1try/webhook2telegram | v2 ghcr.io/muety/webhook2telegram:latest |v3 ghcr.io/muety/telepush
### v2 Link       https://github.com/muety/telepush/pkgs/container/webhook2telegram
### refactoring   https://github.com/muety/telepush/releases/tag/3.0.0

* ( first setup a signal bot , example docker-compose below)
* create  /etc/pam-tg-webhook.conf 
   ```
   export TGWEBBHOOKTOK=1f2f3f4f-3af4-4655-baac-11aaaaabbccc
   export TGWEBBHOOKURL=https://signals.your.dmomain/api/messages
   ```


* example signals bot compose file: 

```
networks:
  default:
    external:
      name: watchdog
services:
  webhook2telegram:
    container_name: signals.watch.yourdomain.lan
    environment:
      APP_MODE: longpolling
      APP_TOKEN: 1234567890:AABcAAAhAaaaa4AAaA_aaaaaaaaa111111
      APP_URL: signals.watch.yourdomain.lan
      LETSENCRYPT_EMAIL: ${LETSENCRYPT_EMAIL:-default-mail-not-set@using-fallback-default.slmail.me}
      LETSENCRYPT_HOST: signals.watch.yourdomain.lan
      NGINX_NETWORK: watchdog
      SSH_PORT: ''
      SSL_POLICY: Mozilla-Modern
      VIRTUAL_HOST: signals.watch.yourdomain.lan
      VIRTUAL_PORT: 8080
      VIRTUAL_PROTO: http
    hostname: signals.watch.yourdomain.lan
#    image: n1try/webhook2telegram
#    image: ghcr.io/muety/webhook2telegram:latest
    image: ghcr.io/muety/telepush    logging:
      driver: json-file
      options:
        max-file: '10'
        max-size: 20m
    ports:
    - 127.0.0.1:55553:8080/tcp
    restart: unless-stopped
    volumes:
    - /storage_global/data/signals.watch.yourdomain.lan/botdata:/srv/data:rw
version: '3.0'
```   

* example .env for the signals bot 
```
APP_URL=signals.watch.yourdomain.lan
APP_MODE=webhook
APP_MODE=longpolling
APP_TOKEN=1234567890:AABcAAAhAaaaa4AAaA_aaaaaaaaa111111
NGINX_NETWORK=watchdog
```

 **hint: bots in multi user chats only see messages beginning with slash ("/") , until you change privacy mode by messaging botmaster so send e.g. "/init" to see it in getUpdates**
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/pam-notify-telegram/README.md/logo.jpg" width="480" height="270"/></div></a>
