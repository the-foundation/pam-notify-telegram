#!/bin/bash
MYPPID=$1
DEBUGFILE="/dev/shm/.DEBUG.TG.PAM_$(id -u)"

CURLRETROPTS="";curlcap=$(curl --help 2>&1);echo "$curlcap" |grep -q retry-all-errors && CURLRETROPTS=" --retry-all-errors";echo "$curlcap" |grep -q retry-connrefuse && CURLRETROPTS="$CURLRETROPTS --retry-connrefused"

send_tgwebhook() {
  [[ "VERIFY_SSL" = "true" ]] && verify=""
  [[ ! -z "${TGWEBBHOOKURL}" ]] && [[ ! -z "${TGWEBBHOOKTOK}" ]] && [[ ! -z "$2" ]] && [[ ! -z "$1" ]] && {
    #echo " sys.log   | LOGGER_TELEGRAM_FOR $PREFIX sending .. "$(echo "$2"|wc -c )" byte" to ${TGWEBBHOOKURL}
      verify="-k"
      
      tgtxt="$2"
      tgtxt=${tgtxt//\'/}
      tgtxt=${tgtxt//\"/}
  ## if debug mode is engaged, save json
  test -e /dev/shm/.DEBUG.TG.PAM &>/dev/null && ( echo '{
  "text": "'$tgtxt'",
  "type": "TEXT",
  "origin": "'$1'"
  }' > "${DEBUGFILE}".json ; chmod go-rw "${DEBUGFILE}".json
  ) &
  OUTFILE=/dev/shm/.pam-tg-notify.out.log
  LOGFILE=/dev/shm/.pam-tg-notify.log
  echo > "$OUTFILE"
  # With the release of version 3, Webhook2Telegram was rebranded to Telepush 
  # Recipient token is now encoded as a path parameter, instead of included inside the payload. Routes have changed, e.g. from /api/messages to /api/messages/<recipient>, while the recipient_token field was removed from a message's JSON schema
  ##txt
  ## v3 ( telepush)
  
  curl_code=$(
  echo '{
  "text": "'$tgtxt'",
  "type": "TEXT",
  "origin": "'$1'"
  }' | curl --fail --retry-max-time 15 --retry 2 --retry-delay 2 $CURLRETROPTS -Lv $verify -X POST ${TGWEBBHOOKURL}/${TGWEBBHOOKTOK} -H "Content-Type: application/json" --data-binary @/dev/stdin -o "$OUTFILE" -w "%{http_code}" 2>$OUTFILE  )
  
  echo "$curl_code"|grep -q ^202$ ||  (
  ## v2 ( webhook2telegram)
  echo '{
  "recipient_token": "'${TGWEBBHOOKTOK}'",
  "text": "'$tgtxt'",
  "type": "TEXT",
  "origin": "'$1'"
  }' | curl --fail --retry-max-time 15 --retry 2 --retry-delay 2 $CURLRETROPTS -Lv $verify -X POST ${TGWEBBHOOKURL}                  -H "Content-Type: application/json" --data-binary @/dev/stdin  -o "$OUTFILE"  2>$OUTFILE 
  
  ) ;
  
  chmod go-rwx "$OUTFILE" "$LOGFILE"
  
  
  echo -n ; } ; ## end params TGWEBBHOOK(URL/TOK) found
echo -n ; } ; # end send_tg_webhook


# helper to convert hex to dec (portable version)
hex2dec(){
    [ "$1" != "" ] && printf "%d" "$(( 0x$1 ))"
}

# expand an ipv6 address
expand_ipv6() {
    ip=$1

    # prepend 0 if we start with :
    echo $ip | grep -qs "^:" && ip="0${ip}"

    # expand ::
    if echo $ip | grep -qs "::"; then
        colons=$(echo $ip | sed 's/[^:]//g')
        missing=$(echo ":::::::::" | sed "s/$colons//")
        expanded=$(echo $missing | sed 's/:/:0/g')
        ip=$(echo $ip | sed "s/::/$expanded/")
    fi

    blocks=$(echo $ip | grep -o "[0-9a-f]\+")
    set $blocks

    printf "%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x\n" \
        $(hex2dec $1) \
        $(hex2dec $2) \
        $(hex2dec $3) \
        $(hex2dec $4) \
        $(hex2dec $5) \
        $(hex2dec $6) \
        $(hex2dec $7) \
        $(hex2dec $8)
}




############# MAIN ####

test -e /dev/shm/.DEBUG.TG.PAM && ( echo  run >> "${DEBUGFILE}") & 


### only "open_session"
#(echo "${PAM_SERVICE}"|grep -e sshd -e dropbear -e mosh ) && [[  ! "$PAM_TYPE" = "open_session" ]] && { 
#  test -e /dev/shm/.DEBUG.TG.PAM && ( echo  NO_open_session >> "${DEBUGFILE}") ;
#  exit 0 ; } ;


##filter git user
test -e /etc/.pam_tg_log-git-user || { 
  echo "$PAM_USER"|grep -q ^git$ &&  exit 0
  echo -n ; } ;


##fork_start
#(

[[ -z "$PAM_USER" ]] && [[ ! -z "$SUDO_USER" ]] && PAM_USER=$SUDO_USER



## filter su ( e.g. su -s /bin/bash "mycommand" www-data )
test -e /etc/.pam-notify-su || { echo "$PAM_SERVICE"|grep -q ^su$ &&   exit 0 ; } ;
test -e /dev/shm/.DEBUG.TG.PAM && ( echo  not_su >> "${DEBUGFILE}") 
test -e /etc/tg-notify-ignored-users && grep "^$PAM_USER$" /etc/tg-notify-ignored-users && exit 0
test -e /etc/tg-notify-ignored-users && grep "^$PAM_RUSER$" /etc/tg-notify-ignored-users && exit 0


test -e /etc/tg-notify-ignored-services && grep "^$PAM_SERVICE$" /etc/tg-notify-ignored-services && exit 0


test -e /dev/shm/.DEBUG.TG.PAM && ( echo  after_filters >> "${DEBUGFILE}") 

#[[ ! -z "$SUDO_USER" ]] && echo "::"$SUDO_USER

MSGTXT=$(

  #env
  echo  "PAM::"${PAM_SERVICE}"::"${PAM_USER}" auth($PAM_TYPE) on: "$(hostname -f)
  echo   '\n'
  echo -n " || *Ruser:* ${PAM_USER}  ${PAM_RUSER} " |grep -v "Ruser:  $";
  echo -n " || *TTY:* ${PAM_TTY} " |sed 's/\/dev\///g';
  )

  test -e /dev/shm/.DEBUG.TG.PAM && ( echo  msg_part_1_done >> "${DEBUGFILE}") 


MSGTXT=$(echo "$MSGTXT";echo)$(
## non-sudo (remote) start
  echo ${PAM_SERVICE} | grep -q -e ssh -e dropbear -e mosh	&& (
       test -e /dev/shm/.DEBUG.TG.PAM && ( echo  NOT_A_SUDO >> "${DEBUGFILE}") 
       ##hostinfo (ipv4)
       #ip=$(echo "${PAM_RHOST}"|sed 's/:[0-9]$//g')
       DEDUP_PAM_RHOST=$( echo "${PAM_RHOST}" |sed 's/^ \+//g;s/ \+$//g'|awk '!x[$0]++' )
       ip="${DEDUP_PAM_RHOST}"
       test -e /dev/shm/.DEBUG.TG.PAM && ( echo  "PAM_RHOST=$DEDUP_PAM_RHOST" >> "${DEBUGFILE}") 
       ipfour_found=unset
       ipsix_found=unset
       if [[ "$ip" =~ ^(([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.){3}([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))$ ]]; then
         ipfour_found=true
         ipsix_found=false
       else
         ipfour_found=false
       fi
       if [[ $ip =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
         ipsix_found=true
       fi
       ## filter private range
       [[ "$ipfour_found" = "true" ]] && (echo "${ip}"|grep -q -e ^127\. -e ^10.\. -e ^192\.168\. -e '^172.1[6-9]\.' -e '^172.2[0-9]\.') && ipfour_found=private

       [[ "$ipsix_found" = "true" ]]  && {
       ## simple trick -> match public ip by All the addresses from the public Internet will be in the range from 2000:: to 3fff:ffff:ffff:ffff:ffff:ffff:ffff:ffff (2000::/3
       ## e.g. ipv6lowest=$( printf "%d" 0x2000);ipv6highest=$( printf "%d" 0x3fff  );decoct=$( printf "%d" 0x$(echo "$ip"|cut -d ":" -f1) ); [[ $decoct -ge $ipv6lowest ]] && [[ $decoct -le $ipv6highest ]] && echo "IS_PUBLIC"
       ipv6lowest=8192
       ipv6highest=16383
       decoct=$( printf "%d" 0x$(echo "$ip"|cut -d ":" -f1) ); 
       [[ -z "$decoct" ]] && decoct=0
            ( [[ $decoct -ge $ipv6lowest ]] && [[ $decoct -le $ipv6highest ]] ) || ipsix_found=private ; } ;
       test -e /dev/shm/.DEBUG.TG.PAM && ( echo  "ipfour_found=$ipfour_found;ipsix_found=$ipsix_found" >> "${DEBUGFILE}") 




       ## print rhost

       [[ "$ipfour_found" = "true" ]] && test -e /etc/.pam_tg_striplastoctet && (echo -n '|| *Rhost:* '${DEDUP_PAM_RHOST} | sed 's/\(\([0-9]\{1,3\}\.\)\{3\}\)[0-9]\{1,3\}/\10/g'|sed -r 's/:[[:xdigit:]]{,4}$/:0/g' |sed 's/.::$//g;' );  
       [[ "$ipfour_found" = "true" ]] && test -e /etc/.pam_tg_striplastoctet || (echo -n '|| *Rhost:* '${DEDUP_PAM_RHOST} | sed 's/:::\+$/::/g;' );  
       [[ "$ipsix_found"  = "true" ]] && test -e /etc/.pam_tg_striplastoctet && (echo -n '|| *Rhost:* '${DEDUP_PAM_RHOST} | sed 's/\(\([0-9]\{1,3\}\.\)\{3\}\)[0-9]\{1,3\}/\10/g'|sed -r 's/:[[:xdigit:]]{,4}$/:0/g' |sed 's/.::$//g;' );  
       [[ "$ipsix_found"  = "true" ]] && test -e /etc/.pam_tg_striplastoctet || (echo -n '|| *Rhost:* '${DEDUP_PAM_RHOST} | sed 's/:::\+$/::/g;' );  
       [[ "$ipsix_found"  = "private"  ]] && test -e /etc/.pam_tg_striplastoctet || (echo -n '|| *Rhost:* '${DEDUP_PAM_RHOST} | sed 's/:::\+$/::/g;' );  
       [[ "$ipfour_found" = "private"  ]] && test -e /etc/.pam_tg_striplastoctet || (echo -n '|| *Rhost:* '${DEDUP_PAM_RHOST} | sed 's/:::\+$/::/g;' );  

       ## print infos about v4
       [[ "$ipfour_found" = "true" ]] && (
          echo -en '\t';
          target=$(echo $ip|awk -F. '{print $4"."$3"." $2"."$1}');
          (            timeout 10 host -t txt $target.sa.senderbase.org       || timeout 5 host -t txt   $target.sa.senderbase.org 1.1.1.1 ) |sed 's/|/\n/g;s/senderbase.org descriptive text.\+|//g'|grep -e ^1= -e 20= -e 53= -e 54= -e 55= -e 50= |sed 's/50=/CITY=/g;s/54=/LAT=/g;s/55=/LON=/g;s/53=/CC=/g;s/^20=/0000=/g;s/^1=/0005=/g;s/^26=/0002=/g;s/^47=/0001=/g'|sort|grep -v 0000=|sed 's/0000=//g;s/0002=/SCORE_DOMAIN=/g;s/0001=/SCORE_IP=/g;s/0005=/ORG=/g;s/^/|/g'|tr -d '\n';  
          echo -n "|"$( ( timeout 10 host        $target.score.senderscore.com   || timeout 5 host      $target.score.senderscore.com 1.1.1.1 ) |grep 127.0.4.|grep -v NXDOMAIN|sed 's/.\+127\.0\.4\.//g;s/^/senderscore: /g' 2>&1 )"|" ;
        ) ## end ip4 was found

       [[ "$ipsix_found" = "true" ]] && (
           #target=$(echo "$ip"|awk '{
           #   gsub(/:/,"")
           #   for (i=length($0); i>0; i--) {
           #       printf "%s.", substr($0,i,1)
           #   }
           #   }')
            CURLRES=$(timeout 5 curl -s -q "https://freeapi.dnslytics.net/v1/ip2asn/$ip"|sed 's/,"/,\n"/g'|grep -e asn -e cidr -e shortname|sed 's/^"//g;s/":\("\|\)/=/g;s/\("\|\),//g;s/^/ | /g')
            [[ -z "$CURLRES" ]] || echo -n "|"${CURLRES}"|"
            [[ -z "$CURLRES" ]] && (
               dnsres=$(host -t txt $(expand_ipv6 $ip|rev|sed 's/://g'|sed 's/./\0./g')"origin6.asn.cymru.com"|cut -d'"' -f2)
               echo "$dnsres"|grep ":" |grep "|" -q && dnsres=$(echo "$dnsres"|sed 's/^/ ASN=/g'|cut -d "|" -f1-4)
               echo -n "|"${dnsres}"|"
            )

        ) ## end ip6 was found
        [[ "$ipfour_found" = "private" ]] && (
          hostsres=$(getent hosts $ip )
          [[ -z "$hostsres" ]] || (
               sendres=$(echo "$hostsres"|sed 's/'$ip//g')
               echo -n "|"${sendres}"|"
          )
          ## maybe try to as  dns in local net here

        ) ## end private v4 found
        [[ "$ipsix_found" = "private" ]] && (
          hostsres=$(getent hosts $ip )
          [[ -z "$hostsres" ]] || (
               sendres=$(echo "$hostsres"|sed 's/'$ip//g')
               echo -n "|"${sendres}"|"
          )
          ## maybe try to as  dns in local net here

        ) ## end private v6 found

        #|sed 's/:[^:]*$/:0/g'
    ) ## end non-sudo (remote)
    echo ${PAM_SERVICE} | grep -q "sudo"  ||  echo '\n'
    echo ${PAM_SERVICE} | grep -q "sudo"  && (test -e /dev/shm/.DEBUG.TG.PAM && ( echo  IS_A_SUDO >> "${DEBUGFILE}")  )
    env|grep SSH_CONN  | sed 's/\(\([0-9]\{1,3\}\.\)\{3\}\)[0-9]\{1,3\}/\10/g'|sed  -r 's/(([[:xdigit:]]{,4}:)*)[[:xdigit:]]{,4}:[[:xdigit:]]{,4}$/\1:/' |sed 's/SSH_CONNECTION=\(.\+\):[^:]* \([0-9]\+\) \(.\+\):[^:].\+$/SSH_CONNECTION=\1:0 \2 \3:0/g;s/SSH_CONNECTION=/SSH_CONN=*/g' |sed 's/.::$//g;'|sed 's/^/|| /g'|grep -v "^||$" |head -c 48 |sed 's/*//'

) # end msgtxt
  
test -e /dev/shm/.DEBUG.TG.PAM && ( echo  msg_part_2_done >> "${DEBUGFILE}") &

MSGTXT=$(echo "$MSGTXT";echo)$(
(  
#echo
test -e /dev/shm/.DEBUG.TG.PAM && ( echo  "SSH_USER_AUTH=$SSH_USER_AUTH" >> "${DEBUGFILE}") &
## native exposed ssh variable

method_sent=no

[[ ! -z "$SSH_AUTH_INFO_0" ]] && { 
  echo "$SSH_AUTH_INFO_0" |sed 's/publickey/ || 🔑 *KyF¹ŋprnŧ* (env ): /g;s/$/ |/g' |head -c 128
echo -n ; } ;
[[ ! -z "$SSH_AUTH_INFO_0" ]] && method_sent=yes

[[ ! -z "$SSH_USER_AUTH" ]] && test -e "$SSH_USER_AUTH" && [[ "$method_sent" = "no" ]] &&   (
    (cat "$SSH_USER_AUTH"|tr -d '\n' ;echo)|sed 's/publickey/ || 🔑 *KyF¹ŋprnŧ* (envf): /g;s/$/ |/g' |head -c 128
    )
[[ ! -z "$SSH_USER_AUTH" ]] && test -e "$SSH_USER_AUTH" && method_sent=yes



## fallback to auth log search ( works only as root )
(echo "${PAM_SERVICE}" | grep -q -e ^dropbear -e ^ssh$  -e sudo ) && [[ "$method_sent" = "no" ]] &&  (
                                  AUTHLOG=/var/log/auth.log
                                  AUTHLOG_NEWEST=$(
                                    find  /var/log -maxdepth 1 -name "auth*" -type f  -mmin -2 &>/dev/null  |grep -v gz$|while read file;do
                                       date -u "+%s $file" --reference=$file;
                                     done|sort -n|tail -n1 |cut -d" " -f2)
                                  [[ ! -z "$AUTHLOG_NEWEST" ]] && AUTHLOG=$AUTHLOG_NEWEST;
                                  test -e /dev/shm/.DEBUG.TG.PAM && ( echo  "search_pubkey by "'sshd\['${MYPPID}'\]'" in $AUTHLOG" >> "${DEBUGFILE}") &
                                  test -e /dev/shm/.DEBUG.TG.PAM && ( ( echo -n "found fingerprint lines:";test -e "$AUTHLOG" && tail -n 300 "$AUTHLOG"  | grep 'sshd\['${MYPPID}'\]' |grep -e "ED25519 key" -e "DSA key" -e "Accepted publickey"|wc -l ) >> "${DEBUGFILE}") &
                                  test -e $AUTHLOG && tail -n 300 $AUTHLOG  | grep 'sshd\['${MYPPID}'\]' |grep -e "ED25519 key" -e "DSA key" -e "Accepted publickey"|sed 's/.\+ssh2: //g;s/^/ || 🔑 *KyF¹ŋprnŧ* (logf): /g;s/ || || / || /g' |sort -u |head -n1 |head -c 128
) ## end ssh/sshd


echo    " || *UTC:*" $(date -u "+%F %H:%M:%S")
#echo '```'
#env
#echo '```'
echo "${PAM_SERVICE}" | grep -q "sudo"  || echo '\n'

  test -e /dev/shm/.DEBUG.TG.PAM && ( echo  msg_part_3_done >> "${DEBUGFILE}") 
#  echo "Service: ${PAM_SERVICE}"
#  echo "Date: `date`"
#  echo "Server: `uname -a`"
) |grep -v ^$| sed 's/<br>/\n/g') ## end MSGTXT=
test -e /dev/shm/.DEBUG.TG.PAM && ( echo  "after_msg_init" >> "${DEBUGFILE}") &

##telegram-notify dislikes underscores ( in markdown it is italic )
MSGTXT="${MSGTXT//_/-}"

test -e /dev/shm/.DEBUG.TG.PAM && ( echo "msg_created" >> "${DEBUGFILE}") &

## filter empty lines

MSGTXT=$(echo "$MSGTXT"|grep -v ^$)

test -e /dev/shm/.DEBUG.TG.PAM && (echo $MSGTXT > /dev/shm/.pam_tg_lastlogintxt_$(id -u) ) &
test -e /dev/shm/.DEBUG.TG.PAM && ( echo running  send_tgwebhook "AUTH@$(hostname -f)" "$MSGTXT" >> "${DEBUGFILE}")
test -e /etc/pam-tg-webhook.conf && {  source /etc/pam-tg-webhook.conf ; res=$( send_tgwebhook "AUTH@$(hostname -f) / $(hostname)" "$MSGTXT" 2>&1 );echo "$res"|grep -q -i "error" && exit 69 ;echo "$res"|grep -q -i "error" || exit 13 ; } ;
test -e /etc/telegram-notify.pam.conf &&  {
        echo $status|grep -q ":sudo:"  && { echo  "$MSGTXT" | /usr/bin/telegram-notify --config /etc/telegram-notify.pam.conf --silent  --warning --text -  && exit 13 ; } ;
##|grep '\[Error\]' || exit 0 ; } ;
       echo $status|grep -q ":sudo:"  ||  { echo  "$MSGTXT" | /usr/bin/telegram-notify --config /etc/telegram-notify.pam.conf --silent --text -              && exit 13 ; } ##|grep '\[Error\]' || exit 0 ; } ;
echo -n ; } ;
###fork end
#) &

exit 0
